openapi: 3.0.1
info:
  title: IJABI Digilib Backend API
  description: Service digunakan untuk menangani request dari mobile app dan sync content dari Nextcloud
  contact:
    email: fatimahnurulinsani@gmail.com
  version: 1.0.0
servers:
- url: https://api.ijabi.org
tags:
- name: resource
  description: Menangani proses sinkronisasi konten dengan Nextcloud
- name: user
  description: Menangani proses user management dari aplikasi mobile
paths:
  /resource:
    post:
      tags:
      - resource
      summary: Membuat konten baru dari Nextcloud
      description: API akan dipanggil oleh Nextcloud saat ada konten baru yang diupload oleh user
      operationId: createResource
      requestBody:
        description: Metadata dari konten
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Resource'
        required: true
      responses:
        201:
          description: Konten berhasil dibuat
          content:
            application/json:
              schema: {}
        401:
          description: Token JWT expired atau role user tidak bisa digunakan untuk membuat konten
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnauthorizedResponse'
        400:
          description: Terdapat kesalahan pada parameter yang dikirim
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
      security:
      - bearerAuth: []
    put:
      tags:
      - resource
      summary: Mengupdate konten dari Nextcloud
      description: API akan dipanggil oleh Nextcloud saat ada update pada konten oleh user
      operationId: updateResource
      requestBody:
        description: Metadata dari konten
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Resource'
        required: true
      responses:
        201:
          description: Konten berhasil diupdate
          content:
            application/json:
              schema: {}
        401:
          description: Token JWT expired atau role user tidak bisa digunakan untuk mengupdate konten
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnauthorizedResponse'
        400:
          description: Terdapat kesalahan pada parameter yang dikirim
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
        404:
          description: ID konten tidak ditemukan
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundResponse'
      security:
      - bearerAuth: []
  /user/is-registered:
    post:
      tags:
      - user
      summary: Melakukan terhadap handphone user dan mengirimkan OTP ke nomor tersebut
      description: Melakukan pengecekan apakah handphone user terdaftar di aplikasi dan mengirimkan OTP ke handphone user baik terdaftar maupun tidak
      operationId: isRegistered
      requestBody:
        description: Nomor handphone user
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PhoneNumberRequest'
        required: true
      responses:
        200:
          description: OTP berhasil dikirimkan
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PhoneRegistrationStatusResponse'
        400:
          description: Nomor handphone tidak valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
  /user/otp:
    post:
      tags:
      - user
      summary: Menangani input OTP dari user
      description: Melakukan pengecekan terhadap OTP yang dikirimkan oleh user pada saat pertama membuka aplikasi
      operationId: submitOTP
      requestBody:
        description: Nomor handphone dan OTP
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SubmitOTPRequest'
        required: true
      responses:
        200:
          description: Kombinasi nomor handphone dan OTP benar
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SubmitOTPResponse'
        400:
          description: Nomor handphone tidak valid atau kombinasi nomor handphone dan OTP salah atau OTP sudah expired
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
  /user/register:
    post:
      tags:
      - user
      summary: Mendaftarkan data user ke aplikasi
      description: Menyimpan data user ke aplikasi setelah user berhasil menginputkan OTP
      operationId: userRegistration
      requestBody:
        description: Data diri user
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserRegistrationRequest'
        required: true
      responses:
        201:
          description: Data user valid dan berhasil disimpan
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/StandardResponse'
        400:
          description: Data user tidak valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
  /user/{user_id}:
    get:
      tags:
      - user
      summary: Mendapatkan informasi user
      description: Mendapatkan informasi user berdasarkan id
      operationId: getUser
      parameters:
        - required: true
          schema:
            title: User Id
            type: integer
          name: user_id
          in: path
      responses:
        200:
          description: Data user berhasil didapatkan
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserInfo'
        404:
          description: User tidak ditemukan
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotFoundResponse'
        401:
          description: Token JWT expired atau role user tidak bisa digunakan untuk melihat data user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnauthorizedResponse'
    post:
      tags:
      - user
      summary: API yang digunakan untuk mengupdate informasi user
      description: Mengupdate informasi user termasuk mengubah firebase_token_id
      operationId: updateUser
      parameters:
        - required: true
          schema:
            title: User Id
            type: string
          name: user_id
          in: path
      requestBody:
        description: JSON kosong
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserInfo'
        required: true
      responses:
        200: 
          description: Informasi user berhasil di-update
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/StandardResponse'
        400:
          description: Terdapat invalid value
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
        401:
          description: Token JWT expired atau role user tidak bisa digunakan untuk mengupdate data user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnauthorizedResponse'
  /images/{image_path}:
    get:
      tags:
      - content
      summary: Mendapatkan file gambar artwork
      description: Mengembalikan file image yang disimpan di /opt/digilib/data/_artworks. Hati-hati dengan path yang diberikan (disanitasi dulu nggak ada .. dsb)!
      operationId: getArtworksFile
      parameters:
        - required: true
          schema:
            title: Image path
            type: string
          name: image_path
          in: path
      responses:
        200: 
          description: File gambar
          content:
            image/*:
              schema:
                type: string
                format: binary
        400:
          description: Jika path tidak sesuai validasi
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
        404:
          description: file tidak ditemukan
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
  /content/book/{book_id}/borrow:
    post:
      tags:
      - content
      summary: API yang digunakan untuk mencatat peminjaman buku
      description: Mencatat peminjaman buku oleh user
      operationId: borrowBook
      parameters:
        - required: true
          schema:
            title: Book Id
            type: string
          name: book_id
          in: path
      requestBody:
        description: JSON kosong
        content:
          application/json:
            schema: {}
        required: true
      responses:
        200: 
          description: File gambar
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/StandardResponse'
        400:
          description: Stok buku sudah habis
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
        404:
          description: Buku tidak ditemukan
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
        401:
          description: Token JWT expired
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnauthorizedResponse'
  /content/book/borrowed:
    get:
      tags:
      - content
      summary: Mendapatkan daftar buku yang dipinjam
      description: Menampilkan daftar buku yang dipinjam beserta tanggal peminjaman dan tanggal berakhir peminjaman
      operationId: getBorrowedBook
      parameters: []
      responses:
        200: 
          description: Daftar buku yang pernah dipinjam
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BorrowedBooks'
        401:
          description: Token JWT expired
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnauthorizedResponse'
  /content/book/{content_id}:
    get:
      tags:
      - content
      summary: Mendapatkan file buku
      description: Memproses folder buku dengan enkripsi, dan mengembalikan file buku dalam .ZIP
      operationId: getBookFile
      parameters:
        - required: true
          schema:
            title: Content Id
            type: string
          name: content_id
          in: path
        - required: true
          schema:
            title: Public Key enkripsi
            type: string
          name: encryption_key
          in: query
      responses:
        200:
          description: "Jika authorization benar dan content_id ada, maka akan mengembalikan file, dengan header Content-Disposition: filename={content_id}.zip dan header Content-Type: "
          content:
            application/zip:
              schema:
                type: string
                format: binary
        404:
          description: Content ID tidak ditemukan atau content tersebut bukan buku
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
        403:
          description: Authorization yang diberikan tidak benar, atau public key yang diberikan bukan public key yang valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
  /content/audio/{content_id}:
    get:
      tags:
      - content
      summary: Mendapatkan file audio
      description: Mengembalikan audio file. Fungsi ini baru dapat dipanggil setelah login (header Authorization diperiksa)
      operationId: getAudioFile
      parameters:
        - required: true
          schema:
            title: Content Id
            type: string
          name: content_id
          in: path
      responses:
        200:
          description: "Jika authorization benar dan content_id ada, maka akan mengembalikan file, dengan header Content-Disposition: filename={content_id}.mp3 dan header Content-Type: audio/mpeg"
          content:
            audio/mpeg:
              schema:
                type: string
                format: binary
        404:
          description: Content ID tidak ditemukan atau content ID tersebut bukan audio
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
        403:
          description: Authorization yang diberikan tidak benar
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BadRequestResponse'
components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    Resources:
      type: array
      items:
        $ref: '#/components/schemas/Resource'
    Resource:
      type: object
      properties:
        id:
          type: string
        path:
          type: string
        file_name:
          type: string
        topic:
          type: string
        author:
          type: string
        people:
          type: string
        event:
          type: string
        year:
          type: string
        date:
          type: string
          format: date
        is_published:
          type: boolean
        is_highlight:
          type: boolean
        featured_image:
          type: string
        copyright:
          type: string
        original_publisher:
          type: string
        source_url:
          type: string
        number_of_stocks:
          type: integer
        content_type:
          type: string
        content_length:
          type: string
        create_date:
          type: string
          format: date
        create_user:
          type: string
        write_date:
          type: string
          format: date
        write_user:
          type: string
    BorrowedBooks:
      type: array
      items:
        $ref: '#/components/schemas/BorrowedBook'
    BorrowedBook:
      type: object
      properties:
        id:
          type: string
        path:
          type: string
        file_name:
          type: string
        topic:
          type: string
        author:
          type: string
        people:
          type: string
        event:
          type: string
        year:
          type: string
        date:
          type: string
          format: date
        is_published:
          type: boolean
        is_highlight:
          type: boolean
        featured_image:
          type: string
        copyright:
          type: string
        original_publisher:
          type: string
        source_url:
          type: string
        number_of_stocks:
          type: integer
        content_type:
          type: string
        content_length:
          type: string
        start_date:
          type: string
        end_date:
          type: string
        borrowing_status:
          type: string
          enum:
            - active
            - inactive
    PhoneNumberRequest:
      type: object
      properties:
        phone_number:
          type: string
      xml:
        name: PhoneNumberRequest
    PhoneRegistrationStatusResponse:
      type: object
      properties:
        is_registered:
          type: boolean
        phone_number:
          type: string
      xml:
        name: PhoneRegistrationStatusResponse
    SubmitOTPRequest:
      type: object
      properties:
        phone_number:
          type: string
        otp:
          type: string
    SubmitOTPResponse:
      type: object
      properties:
        new_user:
          type: boolean
        jwt:
          type: string
    UnauthorizedResponse:
      type: object
      properties:
        message:
          type: string
      xml:
        name: UnauthorizedResponse
    BadRequestResponse:
      type: object
      properties:
        message:
          type: string
      xml:
        name: BadRequestResponse
    NotFoundResponse:
      type: object
      properties:
        message:
          type: string
      xml:
        name: NotFoundRequest
    StandardResponse:
      type: object
      properties:
        status:
          type: string
          enum:
          - success
    UserRegistrationRequest:
      type: object
      properties:
        phone_number:
          type: string
        name:
          type: string
        email:
          type: string
        gender:
          type: string
          enum: 
            - male
            - female
        birth_date:
          type: string
          format: date
        firebase_token_id:
          type: string
      required:
        - phone_number
    UserInfo:
      type: object
      properties:
        id:
          type: integer
        phone_number:
          type: string
        name:
          type: string
        email:
          type: string
        gender:
          type: string
          enum: 
            - male
            - female
        birth_date:
          type: string
          format: date
        firebase_token_id:
          type: string
