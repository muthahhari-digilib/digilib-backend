from django.urls import path
from . import views

urlpatterns = [
    path('home/', views.HomeAPIView.as_view(), name='home'),
    path('home/all/', views.HomeAllAPIView.as_view(), name='all'),
    path('search/', views.SearchAPIView.as_view(), name='search'),

    path('images/<path:filename>', views.get_image, name='get_image'),
    path('content/audio/<path:content_id>', views.get_audio, name='get_audio'),
    path('content/book/<path:book_id>/borrow/', views.BookBorrowingCreateAPIView.as_view(), name='book-borrow'),
    path('content/book/borrowed/', views.BookBorrowedAPIView.as_view(), name='book-borrowed'),
    path('content/book/<path:content_id>', views.get_book, name='get_book'),
    path('content/types', views.get_types, name='get_types'),
    path('content/tags', views.get_tags, name='get_tags'),
]
