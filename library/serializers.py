from rest_framework import serializers

from library.models import BookBorrowingLog, Content
from django.utils import timezone
from account.models import User
from datetime import timedelta


class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = '__all__'
        # fields = ('id', 'content_type', 'file_name', 'path', 'author',
        #           'people_name', 'event_name', 'month', 'year',
        #           'featured_image', 'original_publisher', 'source_url',
        #           'is_highlight')


class BookBorrowingCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookBorrowingLog
        fields = ('id',)

    def create(self, validated_data):
        start_date = timezone.now()
        end_date = start_date + timedelta(days=7)
        book = self.context.get('book')
        user = User.objects.get(username='08999990990')
        validated_data.update({
            'user': user, 'book': book,
            'start_date': start_date, 'end_date': end_date})
            
        if book.number_of_borrowed is None:
            book.number_of_borrowed = 1
        else:
            book.number_of_borrowed += 1
        book.save()
        return super().create(validated_data)


class BookBorrowedSerializer(serializers.ModelSerializer):
    book = ContentSerializer()
    borrowing_status = serializers.SerializerMethodField()

    class Meta:
        model = BookBorrowingLog
        fields = (
            'id', 'book', 'borrowing_status',
            'start_date', 'end_date')

    def to_representation(self, obj):
        """Move fields from book to user representation."""
        representation = super().to_representation(obj)
        book_representation = representation.pop('book')
        for key in book_representation:
            if key == 'id':
                continue
            representation[key] = book_representation[key]

        return representation

    def get_borrowing_status(self, obj):
        now = timezone.now()
        is_active = obj.start_date <= now <= obj.end_date
        return 'active' if is_active else 'inactive'
