from django.db import models
from account.models import User


class Content(models.Model):
    id = models.CharField(primary_key=True, max_length=30)
    content_type = models.CharField(max_length=40)
    file_name = models.CharField(max_length=150)
    description = models.TextField(blank=True)
    path = models.TextField(blank=True)
    author = models.TextField(blank=True, null=True)
    people_name = models.TextField(max_length=150, blank=True, null=True)
    event_name = models.TextField(max_length=150, blank=True, null=True)
    # event_date = models.DateField()
    month = models.TextField(max_length=50, blank=True, null=True)
    # event_year = models.SmallIntegerField()
    year = models.TextField(max_length=50, blank=True, null=True)
    featured_image = models.TextField(blank=True)
    original_publisher = models.CharField(max_length=50, blank=True, null=True)
    source_url = models.TextField(blank=True, null=True)
    is_highlight = models.BooleanField(null=True)
    is_published = models.BooleanField(null=True)
    copyright = models.CharField(max_length=100, blank=True, null=True)
    number_of_borrowed = models.IntegerField(blank=True, null=True)
    number_of_stocks = models.IntegerField(blank=True, null=True)
    content_length = models.IntegerField(blank=True, null=True)
    created_by = models.CharField(max_length=150, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=150, blank=True, null=True)
    modified_at = models.DateTimeField(blank=True, null=True)
    type = models.CharField(max_length=64, blank=True)
    topic = models.CharField(max_length=64, blank=True)
    location = models.TextField(blank=True, null=True)
    topics = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ('-modified_at', '-created_at',)

    def __str__(self):
        return f'{self.author}'


class Alias(models.Model):
    kata = models.CharField(max_length=50)
    alias = models.CharField(max_length=50)

    class Meta:
        ordering = ('kata', 'alias',)

    def __str__(self):
        return f'{self.kata}: {self.alias}'


class BookBorrowingLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE) 
    book = models.ForeignKey(Content, on_delete=models.CASCADE)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    class Meta:
        ordering = ('user', 'book',)

    def __str__(self):
        return f'{self.user}: {self.book}'
