from django.contrib import admin

from library.models import BookBorrowingLog, Content, Alias


class ContentAdmin(admin.ModelAdmin):
    model = Content
    list_display = ('file_name', 'content_type')


class BookBorrowingLogAdmin(admin.ModelAdmin):
    model = BookBorrowingLog
    list_display = ('user', 'start_date')


admin.site.register(Content, ContentAdmin)
admin.site.register(Alias)
admin.site.register(BookBorrowingLog, BookBorrowingLogAdmin)
