from django.contrib.postgres.search import SearchVector
from django.db.models import Q, Func, Value, F
from rest_framework import status
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse, FileResponse
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND

from library.models import Content, Alias, BookBorrowingLog
from library.serializers import BookBorrowedSerializer, BookBorrowingCreateSerializer, ContentSerializer

import os
import json
from datetime import datetime, timedelta
import tempfile

from encrypt import DigilibBookEncryptor
import uuid
from rest_framework_simplejwt.authentication import JWTAuthentication

class ListAPIViewCustom(ListAPIView):    
    # TODO: remove when all clients are up to date
    permission_classes = []
    authentication_classes = []
    # pass
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        status_code = status.HTTP_200_OK
        if request.headers.get("Client-Version") != "0.5.0+9":
            status_code = status.HTTP_204_NO_CONTENT
        # Jika client version sudah 0.5.0+9 maka cek apakah token merupakan JWT atau bukan
        # Jika bukan maka status_code pada response akan 401
        # TODO: jika semua client sudah update maka ganti script dibawah ini dengan permission_classes = (IsAuthenticated,) pada view
        else:
            JWT_authenticator = JWTAuthentication()
            auth_response = JWT_authenticator.authenticate(request)
            if auth_response == None:
                raise PermissionDenied
        
        return Response(serializer.data, status=status_code)

class HomeAPIView(ListAPIViewCustom):
    queryset = Content.objects.filter(is_highlight=True)
    serializer_class = ContentSerializer

    # def get_queryset(self):
    #     return super().get_queryset().filter(is_highlight=True)


class HomeAllAPIView(ListAPIViewCustom):
    queryset = Content.objects.all()
    serializer_class = ContentSerializer

class SearchAPIView(ListAPIViewCustom):
    queryset = Content.objects.all()
    serializer_class = ContentSerializer

    def get_queryset(self):
        q = self.request.GET.get('q')
        type_param = self.request.GET.get('type')

        fq = Q()
        search = None
        if q:
            q_list = q.lower().split(' ')
            kwords = []
            aliases = Alias.objects.filter(alias__in=q_list).\
                values('kata', 'alias')  # alias di db dibuat lowercase semua
            alias_dict = {dt['alias']: dt['kata'] for dt in aliases}
            for idx, w in enumerate(q_list):
                kwords.append(alias_dict.get(w, w))

            q = ' '.join(kwords)
            # print(q)
            
            search = SearchVector(
                Func(
                    F('file_name'),
                    Value('[\/\-\_\;]'), Value(' '), Value('g'),
                    function='regexp_replace',
                ),
                Func(
                    F('description'),
                    Value('[\/\-\_\;]'), Value(' '), Value('g'),
                    function='regexp_replace',
                ),
                Func(
                    F('path'),
                    Value('[\/\-\_\;]'), Value(' '), Value('g'),
                    function='regexp_replace',
                ),
                Func(
                    F('author'),
                    Value('[\/\-\_\;]'), Value(' '), Value('g'),
                    function='regexp_replace',
                ),
                Func(
                    F('topic'),
                    Value('[\/\-\_\;]'), Value(' '), Value('g'),
                    function='regexp_replace',
                ),
                Func(
                    F('event_name'),
                    Value('[\/\-\_\;]'), Value(' '), Value('g'),
                    function='regexp_replace',
                ), Func(
                    F('people_name'),
                    Value('[\/\-\_\;]'), Value(' '), Value('g'),
                    function='regexp_replace',
                ), 'month', 'year', 'id')
            fq = Q(search=q)

        if type_param:
            type_list = list(type_param.split(','))
            fq.add(Q(type__in=type_list), Q.AND)

        if search:
            qset = super().get_queryset().annotate(search=search).filter(fq)
        else:
            qset = super().get_queryset().filter(fq)
        # print(qset.query)
        return qset


class BookBorrowingCreateAPIView(CreateAPIView):
    serializer_class = BookBorrowingCreateSerializer

    def post(self, request, *args, **kwargs):
        book_id = kwargs.get('book_id')
        if not request.headers.get('Authorization'):
            raise PermissionDenied
            
        try:
            self.book = Content.objects.get(id=book_id)
            # book.number_of_stocks
            return super().post(request, *args, **kwargs)
        except Content.DoesNotExist:
            return Response(
                data={'message': 'Kontent tidak ditemukan.'},
                status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({'book': self.book})
        return context


class BookBorrowedAPIView(ListAPIView):
    queryset = BookBorrowingLog.objects.all()
    serializer_class = BookBorrowedSerializer

    def get(self, request, *args, **kwargs):
        # print(request.user)
        if not request.headers.get('Authorization'):
            raise PermissionDenied
        return super().get(request, *args, **kwargs)


@api_view(['GET'])
def get_image(request, filename):
    # prevent path traversal attack
    # https://security.openstack.org/guidelines/dg_using-file-paths.html
    def is_safe_path(basedir, path, follow_symlinks=True):
        import os
        # resolves symbolic links
        if follow_symlinks:
            p = os.path.realpath(path)
            bp = os.path.realpath(basedir)
        else:
            p = os.path.abspath(path)
            bp = os.path.realpath(basedir)

        return p.startswith(basedir) and bp == os.path.commonpath((basedir, p))

    base_path = "/opt/digilib/data/_artworks/"
    # base_path="D:\\Ahmad\\Farj Studio\\Ijabi Dev\\digilib-backend\\"
    # make sure image path is within /opt/digilib/data/_artworks
    image_path = base_path + filename
    if is_safe_path(base_path, image_path, follow_symlinks=False):
        if os.path.exists(image_path):
            f = open(image_path, 'rb')
            return HttpResponse(f, content_type="image/*")
    else:
        return Response(data=dict(message="Requested path is not allowed"), status=HTTP_400_BAD_REQUEST)
    return Response(data=dict(message="Requested path not found"), status=HTTP_404_NOT_FOUND)

@api_view(['GET'])
def get_audio(request, content_id):
    if not request.headers.get('Authorization'):
        return Response(
            data={'message': 'You do not have permission to perform this action.'},
            status=HTTP_403_FORBIDDEN)
    try:
        content = Content.objects.get(id=content_id)
        if content.content_type == 'audio':
            base_path = base_path = "/opt/digilib/data/"
            file_path = base_path + content.file_name

            if os.path.exists(file_path):
                f = open(file_path, 'rb')
                response = FileResponse(f, content_type="audio/mpeg")
                response["Content-Disposition"] = "filename={}.mp3".format(content_id)
                return response
        else:
            return Response(
            data={'message': 'Content type is not audio'},
            status=HTTP_404_NOT_FOUND)
    except Content.DoesNotExist:
        return Response(data=dict(message="Content not found"), status=HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_book(request, content_id):
    KEYPATH = None

    # XXX: peminjaman
    if 'encryption_key' not in request.query_params and not request.headers.get('X-JR-DIGILIB-EncryptionKey'):
         return Response(
            data={'message': 'Encryption key is required'},
            status=HTTP_400_BAD_REQUEST)
    public_key_mobile = request.query_params.get('encryption_key', request.headers.get('X-JR-DIGILIB-EncryptionKey'))
    # PUBLIC_KEY_MOBILE = '{"exponent":"65537","modulo":"16826428398139597273939107200287165241795759952058582727601911098802386630906258315414760076923198894067281002753608424360694752593333590283472541250594880650455643169058009246019801146498182068559671776243054360758992820333648385124098144425731771827382915467391325817810939868420461692400270177229078624730071242353239118686187464575256411942743097338089297200736301058728869317733527398054138887306030513320858195582028885625110734426163952368041544231148351127185677053684963870856190621799711673616255893854168325593113102805363821518996210383491388232596763924046018834028969472568015557917195015168811062999211"}'
    if not request.headers.get('Authorization'):
        return Response(
            data={'message': 'You do not have permission to perform this action.'},
            status=HTTP_403_FORBIDDEN)
    try:
        content = Content.objects.get(id=content_id)
        if content.content_type == 'book':
            base_path = base_path = "/opt/digilib/data/"
            folder_path = base_path + content.file_name
            print('Folder: ' + folder_path)

            if os.path.exists(folder_path):
                import glob
                files = glob.glob(folder_path + "/*.*")
                enc = DigilibBookEncryptor()
                _, outfn = tempfile.mkstemp()

                # XXX: TODO: Peminjaman
                end_pinjam = (datetime.now()+timedelta(days=14)).strftime('%Y-%m-%d')
                res = enc.encrypt(
                    files,
                    end_pinjam,
                    public_key_mobile,
                    outfn
                )
                keys = json.dumps(res.keys)

                # XXX: save ke database dengan user id (peminjaman)
                if KEYPATH:
                    with open(f'{KEYPATH}/key_{content_id}.key', 'w') as f:
                        f.write(keys)
                
                f = open(outfn, 'rb')
                response = FileResponse(f, content_type="application/zip")
                response["Content-Disposition"] = "filename={}.zip".format(content_id)

                #XXX: peminjaman, dan keys tidak dikirim semua
                response["X-JR-DIGILIB-Keys"] = keys
                response["X-JR-DIGILIB-EndPinjam"] = end_pinjam
                return response
            else:
                return Response(
                    data={'message': 'Konten tidak ditemukan di server'},
                    status=HTTP_404_NOT_FOUND)
        else:
            return Response(
            data={'message': 'Content type is not book'},
            status=HTTP_404_NOT_FOUND)
    except Content.DoesNotExist:
        return Response(data=dict(message=f"Content ID '{content_id}' not found"), status=HTTP_404_NOT_FOUND)
    except Exception as e:
        import logging
        logging.error('Unknown error', exc_info=True)
        return Response(data={'message': 'Error: ' + str(e)}, status=500)


@api_view(['GET'])
def get_types(request):
    return Response(
        data = [
            'Buku', 'Audio', 'Video', 'Artikel', 'Karya Seni', 'Foto', 'Instagram'
        ]
    )

@api_view(['GET'])
def get_tags(request):
    # XXX: placeholder
    return Response(
        data = {}
    )
