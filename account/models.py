from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    gender = models.CharField(max_length=1, blank=True)
    phone_number = models.CharField(max_length=15, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    firebase_token_id = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.first_name} {self.phone_number}'


class OtpRequest(models.Model):
    phone_number = models.CharField(max_length=15)
    otp = models.CharField(max_length=10)
    expired_at = models.DateTimeField()
    issued_at = models.DateTimeField(blank=True, null=True)
    request_count = models.SmallIntegerField()

    def __str__(self):
        return f'{self.phone_number}: {self.otp}'
