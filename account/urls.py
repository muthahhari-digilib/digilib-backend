from django.urls import path
# from rest_framework import routers
# from rest_framework.routers import DefaultRouter
from . import views


# router = DefaultRouter()
# router.register(r'', views.UserAPIView, basename='user')

urlpatterns = [
    path('is-registered', views.CheckStatusUserAPIView.as_view(),
         name='check-status-user'),
    path('otp', views.CheckOTPAPIView.as_view(), name='check-otp'),
    path('register', views.RegisterCreateAPIView.as_view(), name='register'),
] # + router.urls


