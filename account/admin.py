from django.contrib import admin

from account.models import User, OtpRequest


class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ('username', 'first_name')


class OtpRequestAdmin(admin.ModelAdmin):
    model = OtpRequest
    list_display = ('phone_number', 'otp')


admin.site.register(User, UserAdmin)
admin.site.register(OtpRequest, OtpRequestAdmin)
