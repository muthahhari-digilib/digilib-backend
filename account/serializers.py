from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from account.models import User


class RegisterSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')
    # firebase_token_id = serializers.CharField()

    class Meta:
        model = User
        fields = (
            'phone_number', 'firebase_token_id', 'email', 'name', 
            'gender', 'birth_date')

    def create(self, validated_data):
        phone_number = validated_data.get('phone_number')
        validated_data.update({'username': phone_number})
        return super().create(validated_data)


class UserInfoSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')

    class Meta:
        model = User
        fields = (
            'id', 'firebase_token_id', 'email', 'name', 
            'gender', 'birth_date', 'phone_number',)


class UserUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='first_name')

    class Meta:
        model = User
        fields = (
            'firebase_token_id', 'email', 'name', 
            'gender', 'birth_date')


class DigilibTokenObtainPairSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        token_data = super().validate(attrs)
        # user_serializer = UserSerializer(self.user)
        data = {'token': token_data}
            # 'user': profile_data}
        return data
