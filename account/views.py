import math
import random
from datetime import timedelta
from uuid import uuid4

from django.conf import settings
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import status
from rest_framework import viewsets
from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView
from twilio.rest import Client
from django.core.exceptions import PermissionDenied

from account.models import User, OtpRequest
from account.serializers import RegisterSerializer, \
    DigilibTokenObtainPairSerializer, UserInfoSerializer, UserUpdateSerializer
from rest_framework import mixins

from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated, BasePermission, AllowAny

def generateOTP():
    # Declare a digits variable
    # which stores all digits
    digits = "0123456789"
    OTP = ""
    # length of password can be chaged
    # by changing value in range
    for i in range(6):
        OTP += digits[math.floor(random.random() * 10)]
    return OTP


def send_sms(otp, no_hp):
    client = Client(settings.ACCOUNT_SID, settings.AUTH_TOKEN)
    body = f'Kode OTP JR Digital Library: {otp}'
    client.messages.create(body=body, from_='+16782939034', to=no_hp)


class CheckStatusUserAPIView(APIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = []
    authentication_classes = []
    http_method_names = ['post']

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        phone_number = request.data.get('phone_number')
        print(phone_number)
        if phone_number is None:
            return Response(data={'message': 'Nomor hp tidak valid.'},
                     status=status.HTTP_400_BAD_REQUEST)

        otp = generateOTP()
        user = None
        try:
            user = User.objects.get(phone_number=phone_number)
            is_registered = True
        except User.DoesNotExist:
            is_registered = False

        expired_at = timezone.now() + timedelta(minutes=30)
        try:
            otp_request = OtpRequest.objects.get(phone_number=phone_number)
            otp_request.otp = otp
            otp_request.expired_at = expired_at
            otp_request.request_count += 1
            otp_request.save()
            # if timezone.now() < otp_request.expired_at:
            #     pass  # kirim otp lagi
        except OtpRequest.DoesNotExist:
            OtpRequest.objects.create(
                phone_number=phone_number, otp=otp,
                expired_at=expired_at, request_count=1)

        data = {
            "is_registered": is_registered,
            "phone_number": phone_number
        }
        send_sms(otp, phone_number)
        
        return Response(data=data, status=status.HTTP_200_OK)


class CheckOTPAPIView(APIView):
    """
    API endpoint that checks OTP and Phone Number combination
    """
    permission_classes = []
    authentication_classes = []
    http_method_names = ['post']

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        phone_number = request.data.get('phone_number')
        otp = request.data.get('otp')
        try:
            otp_request = OtpRequest.objects.get(
                phone_number=phone_number, otp=otp)
            now = timezone.now()
            if otp_request.expired_at < now:
                return Response(
                    data={'message': 'Token sudah expired.'},
                    status=status.HTTP_400_BAD_REQUEST)

            otp_request.issued_at = now
            otp_request.save()
            # update issued_at            

            try:
                user = User.objects.get(phone_number=phone_number)
                new_user = False
                token = RefreshToken.for_user(user)
            except User.DoesNotExist:
                user = User(phone_number)
                token = RefreshToken.for_user(user)
                new_user = True
                
            data = {
                "new_user": new_user,
                "jwt": str(token.access_token)
            }
            return Response(data=data, status=status.HTTP_200_OK)
        except OtpRequest.DoesNotExist:
            return Response(
                data={'message': 'OTP tidak valid.'},
                status=status.HTTP_400_BAD_REQUEST)


class RegisterCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)

    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        phone_number = request.data.get('phone_number')
        if request.user.id != phone_number:
            return Response(
                data={'message': 'Nomor HP yang digunakan berbeda dengan input OTP'},
                status=status.HTTP_400_BAD_REQUEST)

        try:
            User.objects.get(username=phone_number)
            return Response(
                data={'message': 'No HP telah terdaftar.'},
                status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            user = User.objects.get(username=phone_number)
            token = RefreshToken.for_user(user)
            resp = serializer.data
            resp["jwt"]  = str(token.access_token)
            return Response(resp, status=status.HTTP_201_CREATED)

            # r = super().post(request, *args, **kwargs)
            # return 


class UserAPIView(viewsets.ViewSet):

    def retrieve(self, request, pk=None):
        instance = User.objects.get(id=request.user.id)
        serializer_class = UserInfoSerializer
        serializer = serializer_class(instance)
        return Response(serializer.data)

    def update(self, request, pk=None):
        partial = self.kwargs.pop('partial', True)
        instance = User.objects.get(id=request.user.id)
        serializer_class = UserUpdateSerializer
        serializer = serializer_class(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data)

    def dispatch(self, request, *args, **kwargs):
        print(request.headers)
        # user = get_object_or_404(User, pk=kwargs.get('pk'))
        if not request.headers.get('Authorization'):
            raise PermissionDenied
        # if request.user != user:
        #     raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class DigilibTokenObtainPairView(TokenObtainPairView):
    serializer_class = DigilibTokenObtainPairSerializer
